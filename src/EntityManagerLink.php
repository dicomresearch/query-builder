<?php

namespace dicom\kendoUiQueryBuilder;

use dicom\kendoUiQueryBuilder\exceptions\KendoUiException;
use Doctrine\ORM\EntityManager;

/**
 * Class EntityManagerLink
 *
 * ссылка на EntityManager
 * TODO заменить на DI
 * @package dicom\kendoUiQueryBuilder
 */
class EntityManagerLink
{
    /**
     * @var \Doctrine\ORM\EntityManager
     */
    protected static $em;

    /**
     * @return EntityManager
     * @throws KendoUiException
     */
    public static function getEm()
    {
        if (null == static::$em) {
            throw new KendoUiException('You must define Doctrine EntityManager in EntityManagerLinkClass');
        }
        return self::$em;
    }

    /**
     * @param \Doctrine\ORM\EntityManager $em
     */
    public static function setEm(EntityManager $em)
    {
        self::$em = $em;
    }


}