<?php


namespace dicom\kendoUiQueryBuilder\transformation\operators;


use dicom\kendoUiQueryBuilder\transformation\operators\exceptions\MappingException;
use dicom\kendoUiQueryBuilder\transformation\operators\prepareValue\exceptions\ExpressionValuePrepareRegisterException;
use Doctrine\ORM\Query\Expr;

abstract class AbstractExpressionFactory
{

    /**
     * Возвращает готовое к работу Doctrine Expression
     *
     * @param string $alias
     * @param string $operator
     * @param mixed $value
     *
     * @return Expr
     * @throws MappingException
     */
    public function createDoctrineExpression($alias, $operator, $value)
    {
        $textExprFactory = $this->creteExpressionBuilder();
        $textExprMapping = $this->createMapping();

        $exprFactoryFunctionName = $textExprMapping->getFunctionName($operator);

        $value = $this->prepareValue($value);
        $value = $this->additionalPrepareValueIfNecessary($alias, $value);

        return $textExprFactory->$exprFactoryFunctionName($alias, $value);
    }

    /**
     * Can be override for concreate dateTypes
     *
     * @param mixed $value
     * @return mixed
     */
    public function prepareValue($value)
    {
        return $value;
    }

    /**
     * @return Expr
     */
    abstract public function creteExpressionBuilder();

    /**
     * @return AbstractOperatorMapping
     */
    abstract public function createMapping();

    /**
     * @param $alias
     * @param $value
     * @return mixed
     * @throws ExpressionValuePrepareRegisterException
     */
    protected function additionalPrepareValueIfNecessary($alias, $value)
    {
        if (ExpressionValuePrepareRegister::getInstance()->isFieldMustBePrepared($alias)) {
            $preparator = ExpressionValuePrepareRegister::getInstance()->getPreparatorForAlias($alias);
            $value = $preparator->run($value);
            return $value;
        }
        return $value;
    }


}