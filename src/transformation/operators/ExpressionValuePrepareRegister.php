<?php


namespace dicom\kendoUiQueryBuilder\transformation\operators;


use dicom\kendoUiQueryBuilder\transformation\operators\prepareValue\exceptions\ExpressionValuePrepareRegisterException;
use dicom\kendoUiQueryBuilder\transformation\operators\prepareValue\PrepareValueInterface;

/**
 * Class ExpressionValuePrepareRegister
 *
 * Реестр, хранящий модификаторы (препараторы) значений, которые приходят в QueryBuilder.
 * например, можно переопределить, что в значения пришедшие для фильтрации в поле country нужно привести к верхнему регистру
 *
 * @package dicom\kendoUiQueryBuilder\transformation\operators
 */
class ExpressionValuePrepareRegister
{
    /**
     * @var static
     */
    protected static $instance;

    protected $aliasExceptions = [];

    /**
     * @return static
     */
    public static function getInstance()
    {
        if (null === static::$instance) {
            static::$instance = new static();
        }

        return static::$instance;
    }

    protected function __construct()
    {

    }

    public function addAliasForAdditionalPrepare($alias, PrepareValueInterface $preparator)
    {
        $this->aliasExceptions[$alias] = $preparator;
        return $this;
    }

    public function isFieldMustBePrepared($alias)
    {
        return array_key_exists($alias, $this->aliasExceptions);
    }

    /**
     * @param $alias
     * @return PrepareValueInterface
     * @throws ExpressionValuePrepareRegisterException
     */
    public function getPreparatorForAlias($alias)
    {
        if (!array_key_exists($alias, $this->aliasExceptions)) {
            throw ExpressionValuePrepareRegisterException::cantFindAssociationForAlias($alias, $this->aliasExceptions);
        }

        return $this->aliasExceptions[$alias];
    }
}