<?php


namespace dicom\kendoUiQueryBuilder\transformation\operators\prepareValue;

/**
 * Interface PrepareValueInterface
 *
 * Позволяет преобразовывать значения (value) операторов
 *
 * @package transformation\operators\prepareValue
 */
interface PrepareValueInterface
{
    /**
     * Запустить преобразование
     *
     * @param mixed $value
     * @return mixed
     */
    public function run($value);
}