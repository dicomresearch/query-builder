<?php


namespace dicom\kendoUiQueryBuilder\transformation\operators\prepareValue\exceptions;


use dicom\kendoUiQueryBuilder\transformation\exceptions\TransformationLayerException;

class ExpressionValuePrepareRegisterException extends TransformationLayerException
{
    public static function cantFindAssociationForAlias($alias, $registeredAliases)
    {
        return new static(sprintf('Cant fin preparator for alias %s Registered Aliases: %s', $alias, var_export($registeredAliases, true)));
    }
}