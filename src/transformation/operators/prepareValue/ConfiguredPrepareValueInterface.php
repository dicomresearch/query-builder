<?php


namespace dicom\kendoUiQueryBuilder\transformation\operators\prepareValue;

/**
 * Interface ConfiguredPrepareValueInterface
 * @package transformation\operators\prepareValue
 */
interface ConfiguredPrepareValueInterface extends PrepareValueInterface
{
    public function configure($config);
}