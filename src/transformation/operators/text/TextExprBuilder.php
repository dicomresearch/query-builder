<?php


namespace dicom\kendoUiQueryBuilder\transformation\operators\text;


use Doctrine\ORM\Query\Expr;

/**
 * Class TextExprBuilder
 *
 * Сужает Doctrine ExpressionBuilder до операций с текстовыми типами данных.
 * Расширяеят вводом новых операций
 *
 * @package dicom\kendoUiQueryBuilder\transformation\operators\text
 */
class TextExprBuilder extends Expr
{

    public function likeFromBegin($alias, $value)
    {
        return $this->like($alias, $value);
    }

    public function likeEveryPlace($alias, $value)
    {
        return $this->like($alias, $value);
    }

    public function exact($alias, $value)
    {
        return $this->eq($alias, $value);
    }
}