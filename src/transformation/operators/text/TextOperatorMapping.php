<?php


namespace dicom\kendoUiQueryBuilder\transformation\operators\text;


use dicom\kendoUiQueryBuilder\transformation\operators\AbstractOperatorMapping;

class TextOperatorMapping extends AbstractOperatorMapping
{
    protected $kendoToDoctrineMappings = [
        'contains' => 'likeEveryPlace'
    ];

}