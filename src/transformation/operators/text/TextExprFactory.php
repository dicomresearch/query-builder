<?php


namespace dicom\kendoUiQueryBuilder\transformation\operators\text;


use dicom\kendoUiQueryBuilder\transformation\operators\AbstractExpressionFactory;
use dicom\kendoUiQueryBuilder\transformation\operators\exceptions\MappingException;
use Doctrine\ORM\Query\Expr;
use Doctrine\ORM\QueryBuilder;

/**
 * Class TextExprFactory
 *
 * Возвращает сконфигурированный должным образом Doctrine Expression на основании
 * оператора из kendoUI, названия поля и его значения
 *
 * @package dicom\kendoUiQueryBuilder\transformation\operators\text
 */
class TextExprFactory extends AbstractExpressionFactory
{

    public function creteExpressionBuilder()
    {
        return new TextExprBuilder();
    }

    public function createMapping()
    {
        return new TextOperatorMapping();
    }
}
