<?php


namespace dicom\kendoUiQueryBuilder\transformation\operators\numeric;


use dicom\kendoUiQueryBuilder\transformation\operators\AbstractExpressionFactory;

class NumericExprFactory extends AbstractExpressionFactory
{
    public function creteExpressionBuilder()
    {
        return new NumericExprBuilder();
    }

    public function createMapping()
    {
        return new NumericOperationMapping();
    }
}