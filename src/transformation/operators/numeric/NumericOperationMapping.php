<?php


namespace dicom\kendoUiQueryBuilder\transformation\operators\numeric;


use dicom\kendoUiQueryBuilder\transformation\operators\AbstractOperatorMapping;

class NumericOperationMapping extends AbstractOperatorMapping
{
    protected $kendoToDoctrineMappings = [
        'eq' => 'eq',
        'neq' => 'neq',
        'gt' => 'gt',
        'gte' => 'gte',
        'lt' => 'lt',
        'lte' => 'lte',
    ];
}