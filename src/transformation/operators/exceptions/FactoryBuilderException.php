<?php


namespace dicom\kendoUiQueryBuilder\transformation\operators\exceptions;


use dicom\kendoUiQueryBuilder\transformation\exceptions\TransformationLayerException;

class FactoryBuilderException extends TransformationLayerException
{
    public static function cantFindFactoryForOperatorGroup($operatorGroup)
    {
        return new static(sprintf('Can`t find factory for operator group:%s', $operatorGroup));
    }
}