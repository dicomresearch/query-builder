<?php


namespace dicom\kendoUiQueryBuilder\transformation\operators\exceptions;


class ExpressionFactoryBuilderExceptions
{
    public static function factoryForGroupTypeAlreadyRegistered($groupName)
    {
        return new static(sprintf('Expression Factory for groupTypes "%s" already registered', $groupName));
    }

}