<?php


namespace dicom\kendoUiQueryBuilder\transformation\operators\exceptions;



use dicom\kendoUiQueryBuilder\transformation\exceptions\TransformationLayerException;

class MappingException extends TransformationLayerException
{
    public static function relationAlreadyExists($newRelationName, array $relations)
    {
        return new static(sprintf('Relation %s already exists in mapping: %s', $newRelationName, var_export($relations, true)));
    }

    public static function relationDontExists($newRelationName, array $relations)
    {
        return new static(sprintf('Relation %s don`t exists in mapping: %s', $newRelationName, var_export($relations, true)));
    }
}