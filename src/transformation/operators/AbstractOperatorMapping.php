<?php
namespace dicom\kendoUiQueryBuilder\transformation\operators;

use dicom\kendoUiQueryBuilder\transformation\operators\exceptions\MappingException;

abstract class AbstractOperatorMapping {

    protected $kendoToDoctrineMappings = [];

    public function getFunctionName($kendoParamName)
    {
        if (!array_key_exists($kendoParamName, $this->kendoToDoctrineMappings)) {
            throw MappingException::relationDontExists($kendoParamName, $this->kendoToDoctrineMappings);
        }

        return $this->kendoToDoctrineMappings[$kendoParamName];
    }

    public function addRelation($kendoParamName, $textFactoryFunction)
    {
        if (array_key_exists($kendoParamName, $this->kendoToDoctrineMappings)) {
            throw MappingException::relationAlreadyExists($kendoParamName, $this->kendoToDoctrineMappings);
        }

        $this->kendoToDoctrineMappings[$kendoParamName] = $textFactoryFunction;
    }

    public function removeRelation($kendoParamName)
    {
        if (!array_key_exists($kendoParamName, $this->kendoToDoctrineMappings)) {
            throw MappingException::relationDontExists($kendoParamName, $this->kendoToDoctrineMappings);
        }

        unset($this->kendoToDoctrineMappings[$kendoParamName]);
    }

    public function replaceRelation($kendoParamName, $textFactoryFunction)
    {
        $this->kendoToDoctrineMappings[$kendoParamName] = $textFactoryFunction;
    }

}