<?php


namespace dicom\kendoUiQueryBuilder\transformation\operators;


use dicom\kendoUiQueryBuilder\transformation\operators\date\DateExprFactory;
use dicom\kendoUiQueryBuilder\transformation\operators\dateTime\DateTimeExprFactory;
use dicom\kendoUiQueryBuilder\transformation\operators\exceptions\ExpressionFactoryBuilderExceptions;
use dicom\kendoUiQueryBuilder\transformation\operators\exceptions\FactoryBuilderException;
use dicom\kendoUiQueryBuilder\transformation\operators\logic\LogicExpressionFactory;
use dicom\kendoUiQueryBuilder\transformation\operators\numeric\NumericExprFactory;
use dicom\kendoUiQueryBuilder\transformation\operators\text\TextExprFactory;

/**
 * Class ExpressionFactoryBuilder
 * @package dicom\kendoUiQueryBuilder\transformation\operators
 */
class ExpressionFactoryBuilder
{
    protected $expressionFactories = [];

    /**
     * @var static
     */
    protected static $instance;

    /**
     * @return static
     */
    public static function getInstance()
    {
        if (null === static::$instance) {
            static::$instance = new static();
        }

        return static::$instance;
    }

    protected function __construct()
    {
        $this->createDefaultFactories();
    }

    /**
     * @param $forOperatorGroup
     *
     * @return AbstractExpressionFactory
     * @throws FactoryBuilderException
     */
    public function createExprFactory($forOperatorGroup)
    {
        $forOperatorGroup = strtolower($forOperatorGroup);
        if (! $this->hasFactoryForGroup($forOperatorGroup)) {
            throw FactoryBuilderException::cantFindFactoryForOperatorGroup($forOperatorGroup);
        }

        return $this->expressionFactories[$forOperatorGroup];
    }

    public function hasFactoryForGroup($forGroupName)
    {
        return array_key_exists($forGroupName, $this->expressionFactories);
    }

    /**
     * @param string $forGroupName
     * @param AbstractExpressionFactory $expressionFactory
     *
     * @return $this
     * @throws ExpressionFactoryBuilderExceptions
     */
    public function registerExpressionFactory($forGroupName, AbstractExpressionFactory $expressionFactory)
    {
        if (array_key_exists($forGroupName, $expressionFactory)) {
            throw ExpressionFactoryBuilderExceptions::factoryForGroupTypeAlreadyRegistered($forGroupName);
        }

        $this->expressionFactories[$forGroupName] = $expressionFactory;

        return $this;
    }

    public function overrideFactory($groupName, AbstractExpressionFactory $expressionFactory)
    {
        $this->expressionFactories[$groupName] = $expressionFactory;
    }

    protected function createDefaultFactories()
    {
        $this
            ->registerExpressionFactory('datetime', new DateTimeExprFactory())
            ->registerExpressionFactory('logic',    new LogicExpressionFactory())
            ->registerExpressionFactory('numeric',  new NumericExprFactory())
            ->registerExpressionFactory('text',     new TextExprFactory());
    }
}