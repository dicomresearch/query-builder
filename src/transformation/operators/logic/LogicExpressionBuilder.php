<?php


namespace dicom\kendoUiQueryBuilder\transformation\operators\logic;


use Doctrine\ORM\Query\Expr;

/**
 * Class LogicExpressionBuilder
 *
 * Предоставляет удобный интерфейс к логическим операторам
 *
 * @package dicom\kendoUiQueryBuilder\transformation\operators\logic
 */
class LogicExpressionBuilder extends Expr
{
    /**
     * В отличии от обычного orX принимает значения в ввиде массива
     *
     * @param array $x
     *
     * @return Expr\Orx
     */
    public function orXArray(array $x)
    {
        return call_user_func_array([$this, 'orX'], $x);
    }

    /**
     * В отличии от обычного andX принимает значения в виде массива
     *
     * @param array $x
     *
     * @return Expr\Andx
     */
    public function andXArray(array $x)
    {
        return call_user_func_array([$this, 'andX'], $x);
    }


}