<?php


namespace dicom\kendoUiQueryBuilder\transformation\operators\logic;


use dicom\kendoUiQueryBuilder\transformation\operators\AbstractExpressionFactory;

class LogicExpressionFactory extends AbstractExpressionFactory
{

    //todo поидее должен быть наследником createDoctrineExpression
    public function createDoctrineLogicalExpression($x, $operator)
    {
        $textExprFactory = $this->creteExpressionBuilder();
        $textExprMapping = $this->createMapping();

        $exprFactoryFunctionName = $textExprMapping->getFunctionName($operator);

        return $textExprFactory->$exprFactoryFunctionName($x);
    }

    public function creteExpressionBuilder()
    {
        return new LogicExpressionBuilder();
    }

    public function createMapping()
    {
        return new LogicOperatorMapping();
    }
}