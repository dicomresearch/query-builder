<?php


namespace dicom\kendoUiQueryBuilder\transformation\operators\logic;


use dicom\kendoUiQueryBuilder\transformation\operators\AbstractOperatorMapping;

class LogicOperatorMapping extends AbstractOperatorMapping
{
    protected $kendoToDoctrineMappings = [
        'and' => 'andXArray',
        'or' => 'orXArray',
    ];
}