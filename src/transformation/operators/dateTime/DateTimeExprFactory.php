<?php


namespace dicom\kendoUiQueryBuilder\transformation\operators\dateTime;

use dicom\kendoUiQueryBuilder\transformation\operators\AbstractExpressionFactory;
use dicom\kendoUiQueryBuilder\transformation\operators\dateTime\prepareValue\ConvertValueToDateTime;

/**
 * Class DateTimeExprFactory
 *
 * Используется когда данные в таблицы лежат вместе со временем, а операции надо производить только с датой
 *
 * @package dicom\kendoUiQueryBuilder\transformation\operators\dateTime
 */
class DateTimeExprFactory extends AbstractExpressionFactory
{
    public function creteExpressionBuilder()
    {
        return new DateTimeExprBuilder();
    }

    public function createMapping()
    {
        return new DateTimeOperatorMapping();
    }

    public function prepareValue($value)
    {
        $converter = new ConvertValueToDateTime();
        $value = $converter->run($value);

        return parent::prepareValue($value);
    }


}