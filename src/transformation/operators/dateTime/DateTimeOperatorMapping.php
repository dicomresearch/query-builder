<?php


namespace dicom\kendoUiQueryBuilder\transformation\operators\dateTime;


use dicom\kendoUiQueryBuilder\transformation\operators\AbstractOperatorMapping;

class DateTimeOperatorMapping extends AbstractOperatorMapping
{
    protected $kendoToDoctrineMappings = [
        'eq' => 'dateTimeBetween',
        'lt' => 'dateTimeLt',
        'lte' => 'dateTimeLte',
        'gt' => 'dateTimeGt',
        'gte' => 'dateTimeGte',
        'neq' => 'dateTimeNotBetween'
    ];
}