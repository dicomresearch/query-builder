<?php


namespace dicom\kendoUiQueryBuilder\transformation\operators\dateTime\prepareValue;



use dicom\kendoUiQueryBuilder\transformation\operators\prepareValue\PrepareValueInterface;

class ConvertValueToDateTime implements PrepareValueInterface
{
    public function run($value)
    {
        $dateValue = new \DateTime();
        $dateValue->setTimestamp($value);

        return $dateValue;
    }
}