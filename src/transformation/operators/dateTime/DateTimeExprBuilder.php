<?php


namespace dicom\kendoUiQueryBuilder\transformation\operators\dateTime;


use Doctrine\ORM\Query\Expr;

class DateTimeExprBuilder extends Expr
{

    /**
     * eq
     *
     * @param $alias
     * @param \DateTime $value
     * @return Expr\Func
     */
    public function dateTimeBetween($alias, \DateTime $value)
    {
        return $this->between($alias, "'".$value->format('Y-m-d 00:00:00')."'", "'".$value->format('Y-m-d 23:59:59')."'");
    }

    public function dateTimeNotBetween($alias, \DateTime $value)
    {
        return $this->notBetween($alias, "'".$value->format('Y-m-d 00:00:00')."'", "'".$value->format('Y-m-d 23:59:59')."'");
    }

    /**
     * Creates an instance of NOT BETWEEN() function, with the given argument.
     *
     * @param mixed   $val Valued to be inspected by range values.
     * @param integer $x   Starting range value to be used in NOT BETWEEN() function.
     * @param integer $y   End point value to be used in NOT BETWEEN() function.
     *
     * @return Expr\Func A BETWEEN expression.
     */
    public function notBetween($val, $x, $y)
    {
        return $val . ' NOT BETWEEN ' . $x . ' AND ' . $y;
    }

    public function dateTimeLt($alias, \DateTime $value)
    {
        $date = $value->format('Y-m-d 00:00:00');
        return $this->lt($alias, "'".$date."'");
    }

    public function dateTimeGt($alias, \DateTime $value)
    {
        $date = $value->format('Y-m-d 23:59:59');
        return $this->gt($alias, "'".$date."'");
    }

    public function dateTimeLte($alias, \DateTime $value)
    {
        $date = $value->format('Y-m-d 23:59:59');
        return $this->lte($alias, "'".$date."'");
    }

    public function dateTimeGte($alias, \DateTime $value)
    {
        $date = $value->format('Y-m-d 00:00:00');
        return $this->gte($alias, "'".$date."'");
    }


}