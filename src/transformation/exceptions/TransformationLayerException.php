<?php


namespace dicom\kendoUiQueryBuilder\transformation\exceptions;



use dicom\kendoUiQueryBuilder\exceptions\KendoUiException;

abstract class TransformationLayerException extends KendoUiException
{

}