<?php


namespace dicom\kendoUiQueryBuilder\transformation\exceptions;


use dicom\kendoUiQueryBuilder\transformation\exceptions\TransformationLayerException;

class GroupMapperException extends TransformationLayerException
{
    public static function typeNotFoundIntoGroups($needleType, $groupsMapping)
    {
        return new static(sprintf(
            'cat`t find type: %s into groups mapping: %s', $needleType, var_export($groupsMapping, true)
        ));
    }

    public static function groupAlreadyExists($groupName, $registeredGroups)
    {
        return new static(sprintf('Group %s already exists into mapping: %s', $groupName, var_export($registeredGroups, true)));
    }

    public static function groupDontRegistered($groupName, $registeredGroups)
    {
        return new static(sprintf('Group "%s" dont registered yet, registered groups: "%s"', $groupName, var_export($registeredGroups, true)));
    }
}