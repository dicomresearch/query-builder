<?php


namespace dicom\kendoUiQueryBuilder\transformation\exceptions;


class OverrideFieldTypeRegisterException
{
    public static function cantFindFieldIntoOverrideRegister($fieldName, $registeredFields)
    {
        return new static(sprintf(
            'cant find field with alias %s into Override Field Type Register
            Registerred field: %s',
            $fieldName,
            var_export($registeredFields, true)
        ));
    }
}