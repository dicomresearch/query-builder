<?php


namespace dicom\kendoUiQueryBuilder\transformation;


use dicom\kendoUiQueryBuilder\queryObjectRepresentation\kendoCriteria\filter\CompareFilter;
use dicom\kendoUiQueryBuilder\transformation\exceptions\GroupMapperException;
use dicom\kendoUiQueryBuilder\transformation\operators\ExpressionFactoryBuilder;

class DoctrineExpressionFactory
{
    /**
     * Create Doctrine Expression for filter
     *
     * @param CompareFilter $filter
     *
     * @return \Doctrine\ORM\Query\Expr
     *
     * @throws GroupMapperException
     * @throws operators\exceptions\FactoryBuilderException
     */
    public function createExpr(CompareFilter $filter)
    {
        $fieldType = $this->detectedFieldType($filter);

        $groupMapper = GroupMapper::getInstance();
        $exprGroup = $groupMapper->getGroup($fieldType);

        $exprBuilder = ExpressionFactoryBuilder::getInstance();
        $exprFactory = $exprBuilder->createExprFactory($exprGroup);

        $expr = $exprFactory->createDoctrineExpression(
            $filter->getField()->getAlias(),
            $filter->getOperator(),
            $filter->getValue()
        );

        return $expr;
    }

    /**
     * @param CompareFilter $filter
     * @return \Doctrine\DBAL\Types\Type|null|string
     * @throws exceptions\OverrideFieldTypeRegisterException
     */
    protected function detectedFieldType(CompareFilter $filter)
    {
        $overrideRegister = OverrideFieldTypeRegister::getInstance();
        $fieldType = $filter->getField()->getTableColumnType();
        if ($overrideRegister->isFieldMustBeOverride($filter->getField())) {
            $fieldType = $overrideRegister->getOverrideFieldType($filter->getField());
            return $fieldType;
        }
        return $fieldType;
    }
}