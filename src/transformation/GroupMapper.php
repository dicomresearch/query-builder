<?php


namespace dicom\kendoUiQueryBuilder\transformation;


use dicom\kendoUiQueryBuilder\transformation\exceptions\GroupMapperException;

class GroupMapper
{
    /**
     * @var static
     */
    protected static $instance;

    protected $map = [
        'numeric' => ['integer', 'bigint'],
        'datetime' => ['datetime'],
        'text' => ['text', 'varchar', 'string'],
    ];

    /**
     * @return GroupMapper
     */
    public static function getInstance()
    {
        if (null === static::$instance) {
            static::$instance = new static();
        }

        return static::$instance;
    }

    protected function __construct() {}

    public function getGroup($type)
    {
        foreach ($this->map as $groupName => $typesIntoGroup) {
            if (in_array($type, $typesIntoGroup)) {
                return $groupName;
            }
        }

        throw GroupMapperException::typeNotFoundIntoGroups($type, $this->map);
    }

    public function addGroup($groupName, array $dataTypes = [])
    {
        if (array_key_exists($groupName, $this->map)) {
            throw GroupMapperException::groupAlreadyExists($groupName, $dataTypes);
        }

        $this->map[$groupName] =  $dataTypes;
        return $this;
    }

    public function replaceOrAddGroup($groupName, array $dataTypes)
    {
        if ($this->hasGroup($groupName)) {
            $this->removeGroup($groupName);
        }

        $this->addGroup($groupName, $dataTypes);
        return $this;
    }

    public function addTypeToGroup($groupName, $type, $createGroupIfNecessary = true)
    {
        if ($this->hasGroup($groupName)) {
            $this->map[$groupName][] = $type;
            return $this;
        }

        if (!$this->hasGroup($groupName) && $createGroupIfNecessary === true) {
            $this->addGroup($groupName, [$type]);
            return $this;
        }

        throw GroupMapperException::groupDontRegistered($groupName, $this->map);
    }

    public function hasGroup($groupName)
    {
        return array_key_exists($groupName, $this->map);
    }

    public function removeGroup($groupName)
    {
        if (!$this->hasGroup($groupName)) {
            throw GroupMapperException::groupDontRegistered($groupName, $this->map);
        }

        unset($this->map[$groupName]);
    }

    public function getTypesIntoGroup($groupName)
    {
        if (!$this->hasGroup($groupName)) {
            throw GroupMapperException::groupDontRegistered($groupName, $this->map);
        }

        return $this->map[$groupName];
    }




}