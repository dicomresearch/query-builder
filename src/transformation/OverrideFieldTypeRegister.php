<?php


namespace dicom\kendoUiQueryBuilder\transformation;
use dicom\kendoUiQueryBuilder\queryObjectRepresentation\field\Field;
use dicom\kendoUiQueryBuilder\transformation\exceptions\OverrideFieldTypeRegisterException;


/**
 * Class SpecialFieldMappingRegister
 *
 * В процессе выполнения здесь регистрируютс поля, для которых нужно построит Expr с типом данных,
 * отличающимся от типа данных в БД.
 *
 * @package dicom\kendoUiQueryBuilder\transformation
 */
class OverrideFieldTypeRegister
{
    /**
     * @var static
     */
    protected static $instance;

    protected $aliasExceptions = [];

    /**
     * @return OverrideFieldTypeRegister
     */
    public static function getInstance()
    {
        if (null === static::$instance) {
            static::$instance = new static();
        }

        return static::$instance;
    }

    protected function __construct()
    {

    }

    public function addAliasForOverride($alias, $overrideType)
    {
        $this->aliasExceptions[$alias] = $overrideType;
    }

    public function isFieldMustBeOverride(Field $field)
    {
        return array_key_exists($field->getAlias(), $this->aliasExceptions);
    }

    public function getOverrideFieldType(Field $field)
    {
        if (!array_key_exists($field->getAlias(), $this->aliasExceptions)) {
            throw OverrideFieldTypeRegisterException::cantFindFieldIntoOverrideRegister($field->getAlias(), $this->aliasExceptions);
        }

        return $this->aliasExceptions[$field->getAlias()];
    }
}