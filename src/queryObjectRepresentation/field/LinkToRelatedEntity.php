<?php


namespace dicom\kendoUiQueryBuilder\queryObjectRepresentation\field;

use Doctrine\ORM\Mapping\ClassMetadata;

/**
 * Class LinkToRelatedEntity
 *
 * Применяется когда поле находится не в искомой таблице, а подключается с использованием JOIN
 *
 * таким образом LinkToRelatedEntity хранит ссылку на простое поле (в другой таблице)
 *
 * @package dicom\kendoUiQueryBuilder\queryObjectRepresentation\field
 */
class LinkToRelatedEntity extends Field
{
    /**
     * @var Field
     */
    protected $subField;

    /**
     * Название таблицы, к которому относится это поле
     *
     * @var string
     */
    protected $tableName;

    public function __construct($relationName, ClassMetadata $classMetadata)
    {
        parent::__construct($this->getFieldNameFromAlias($relationName), $classMetadata);

        $this->setTableName($this->getTableNameFromAlias($relationName));
        $this->createSubField($relationName);
    }

    /**
     * @return Field
     */
    public function getSubField()
    {
        return $this->subField;
    }

    /**
     * @param Field $subField
     */
    public function setSubField($subField)
    {
        $this->subField = $subField;
    }

    /**
     * @return mixed
     */
    public function getTableColumnType()
    {
        return $this->getSubField()->getTableColumnType();
    }

    /**
     * return tableName.fieldName
     * @return string
     */
    public function getAlias()
    {
        $alias = $this->getTableName() .'.'. $this->getSubField()->getName();
        return $alias;
    }


    /**
     * @return string
     */
    public function getTableName()
    {
        return $this->tableName;
    }

    /**
     * @param string $tableName
     */
    public function setTableName($tableName)
    {
        $this->tableName = $tableName;
    }

    /**
     * @param $alias
     * @return string
     */
    public function getFieldNameFromAlias($alias)
    {
        $aliasAsArray = explode('.', $alias);
        $tableName = array_shift($aliasAsArray);
        $relationName = implode('.', $aliasAsArray);
        return $relationName;
    }

    public function getTableNameFromAlias($alias)
    {
        $aliasAsArray = explode('.', $alias);
        $tableName = reset($aliasAsArray);
        return $tableName;
    }

    /**
     * @param $relationName
     */
    protected function createSubField($relationName)
    {
        $associationToTable = $this->getTableNameFromAlias($relationName);
        $associationToClass = $this->getClassMetadata()->getAssociationTargetClass($associationToTable);
        $subClassMetadata = \Yii::app()->pacsOrm->entityManager->getClassMetadata($associationToClass );
        $this->setSubField(new SingleField($this->getFieldNameFromAlias($relationName), $subClassMetadata));
    }


}