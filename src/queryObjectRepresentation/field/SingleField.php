<?php


namespace dicom\kendoUiQueryBuilder\queryObjectRepresentation\field;


class SingleField extends Field
{

    /**
     * Return TableName.ColumnName
     * @return string
     */
    public function getAlias()
    {
        return $this->classMetadata->getTableName() .'.'. $this->getName();
    }


}