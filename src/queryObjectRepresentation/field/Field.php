<?php


namespace dicom\kendoUiQueryBuilder\queryObjectRepresentation\field;


use Doctrine\ORM\Mapping\ClassMetadata;

abstract class Field
{

    /**
     * @var string
     */
    protected $name;

    /**
     * @var ClassMetadata
     */
    protected $classMetadata;

    public function __construct($fieldName, $cm)
    {
        $this->setClassMetadata($cm);
        $this->setName($fieldName);
    }

    abstract public function getAlias();

    /**
     * Вернуть тип колонки, к которойприменен фильтр
     */
    public function getTableColumnType()
    {
        return $this->classMetadata->getTypeOfField($this->getName());
    }

    public function getTableName()
    {
        return $this->getClassMetadata()->getTableName();
    }


    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return ClassMetadata
     */
    public function getClassMetadata()
    {
        return $this->classMetadata;
    }

    /**
     * @param ClassMetadata $classMetadata
     */
    public function setClassMetadata(ClassMetadata $classMetadata)
    {
        $this->classMetadata = $classMetadata;
    }


}