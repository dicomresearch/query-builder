<?php


namespace dicom\kendoUiQueryBuilder\queryObjectRepresentation\field;


use dicom\kendoUiQueryBuilder\EntityManagerLink;
use Doctrine\ORM\EntityManager;

class FieldBuilder
{
    /** @var  EntityManager */
    public $em;

    /**
     * @var string
     */
    private $entityName;

    public function __construct($entityName)
    {
        $this->entityName = $entityName;
    }

    public function createField($fieldNameOrRelationName)
    {

        if (strpos($fieldNameOrRelationName, '.') === false) {
            $field = new SingleField($fieldNameOrRelationName, $this->getEm()->getClassMetadata($this->entityName));
        } else {
            $field = new LinkToRelatedEntity($fieldNameOrRelationName, $this->getEm()->getClassMetadata($this->entityName));
        }

        return $field;
    }

    /**
     * @return EntityManager
     */
    protected function getEm()
    {
        return EntityManagerLink::getEm();
    }


}