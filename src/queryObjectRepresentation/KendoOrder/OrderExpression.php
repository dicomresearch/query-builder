<?php


namespace dicom\kendoUiQueryBuilder\queryObjectRepresentation\KendoOrder;


use dicom\kendoUiQueryBuilder\queryObjectRepresentation\field\Field;

class OrderExpression
{
    /**
     * @var Field
     */
    protected $field;

    protected $direction;

    public function __construct(Field $field, $direction = 'desc')
    {
        $this->setField($field);
        $this->setDirection($direction);
    }

    /**
     * @return Field
     */
    public function getField()
    {
        return $this->field;
    }

    /**
     * @param Field $field
     */
    public function setField($field)
    {
        $this->field = $field;
    }

    /**
     * @return mixed
     */
    public function getDirection()
    {
        return $this->direction;
    }

    /**
     * @param mixed $direction
     */
    public function setDirection($direction)
    {
        $this->direction = $direction;
    }



}