<?php


namespace dicom\kendoUiQueryBuilder\queryObjectRepresentation\KendoOrder;


use dicom\kendoUiQueryBuilder\queryObjectRepresentation\field\FieldBuilder;

/**
 * Class OrderExpressionBuilder
 *
 * Построитель выражений сортировки
 *
 * @package queryObjectRepresentation\KendoOrder
 */
class OrderExpressionBuilder
{
    protected $entityName;

    public function __construct($entityName)
    {
        $this->setEntityName($entityName);
    }

    /**
     * Создать несколько OrderExpression
     *
     * @param array $arrayOfOrderBy
     * @return OrderExpression[]
     */
    public function createManyOrderBy(array $arrayOfOrderBy)
    {
        $orderExpressions = [];
        foreach ($arrayOfOrderBy as $orderBy) {
            $orderExpressions[] = $this->createOrderByExpression($orderBy);
        }

        return $orderExpressions;
    }

    /**
     * Создать один OrderExpression - на вход одно выражение сортировки
     *
     * @param array $orderBy
     * @return OrderExpression
     */
    public function createOrderByExpression(array $orderBy)
    {
        $fieldBuilder = new FieldBuilder($this->getEntityName());
        $field = $fieldBuilder->createField($orderBy['field']);

        $orderExpression = new OrderExpression($field, $orderBy['dir']);

        return $orderExpression;
    }

    /**
     * @return mixed
     */
    public function getEntityName()
    {
        return $this->entityName;
    }

    /**
     * @param mixed $entityName
     */
    public function setEntityName($entityName)
    {
        $this->entityName = $entityName;
    }



}