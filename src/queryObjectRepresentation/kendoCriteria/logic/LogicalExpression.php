<?php


namespace dicom\kendoUiQueryBuilder\queryObjectRepresentation\kendoCriteria\logic;


use dicom\kendoUiQueryBuilder\queryObjectRepresentation\kendoCriteria\filter\CompareFilter;

/**
 * Class LogicalExpression
 *
 * Логическое выражение соединяет несколько других выражения оператором Or или And
 *
 * @package dicom\kendoUiQueryBuilder\queryObjectRepresentation\kendoCriteria\logic
 */
class LogicalExpression
{
    /**
     * or, and
     *
     * @var string
     */
    protected $logicalOperator;

    /**
     * @var CompareFilter
     */
    protected $filters = [];

    public function __construct($logicalOperator)
    {
        $this->setLogicalOperator($logicalOperator);
    }

    /**
     * @return CompareFilter
     */
    public function getFilters()
    {
        return $this->filters;
    }

    public function hasFilters()
    {
        return !empty($this->filters);
    }

    /**
     * @param CompareFilter|LogicalExpression $filter
     */
    public function addFilter($filter)
    {
        $this->filters[] = $filter;
    }

    /**
     * @return string
     */
    public function getLogicalOperator()
    {
        return $this->logicalOperator;
    }

    /**
     * @param string $logicalOperator
     */
    public function setLogicalOperator($logicalOperator)
    {
        $this->logicalOperator = $logicalOperator;
    }



}