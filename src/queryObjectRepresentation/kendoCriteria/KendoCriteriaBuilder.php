<?php

namespace dicom\kendoUiQueryBuilder\queryObjectRepresentation\kendoCriteria;

use dicom\kendoUiQueryBuilder\queryObjectRepresentation\field\FieldBuilder;
use dicom\kendoUiQueryBuilder\queryObjectRepresentation\kendoCriteria\filter\CompareFilter;
use dicom\kendoUiQueryBuilder\queryObjectRepresentation\kendoCriteria\logic\LogicalExpression;

class KendoCriteriaBuilder
{
    /**
     * Для какой сущности собираем криетрию (основная, не считаем те, которые Join'ятся)
     *
     * @var string
     */
    protected $entityName;

    public function __construct($entityName)
    {
        $this->entityName = $entityName;
    }

    public function buildKendoCriteria(array $criteria)
    {
        if (array_key_exists('logic', $criteria)) {
            $kendoCriteria =$this->buildLogicExpression($criteria);
        } else {
            $kendoCriteria = $this->buildFilter($criteria);
        }

        return $kendoCriteria;
    }

    protected function buildLogicExpression($logicalCriteria)
    {
        $kendoCriteria = new LogicalExpression($logicalCriteria['logic']);

        foreach ($logicalCriteria['filters'] as $filterOrLogicalExpr)
        {
            $obj = $this->buildKendoCriteria($filterOrLogicalExpr);
            $kendoCriteria->addFilter($obj);//todo может быть не фильтр, а logicExpr, привести к одному интерфейсу их
        }

        return $kendoCriteria;
    }

    protected function buildFilter(array $filterCriteria)
    {
        $fieldBuilder = new FieldBuilder($this->entityName);
        $field = $fieldBuilder->createField($filterCriteria['field']);

        $value = $filterCriteria['value'];

        if ($filterCriteria['operator'] == 'contains') {
            $value = '%' . $value . '%';
        }

        $filter = new CompareFilter(
            $field,
            $filterCriteria['operator'],
            $value
        );

        return $filter;
    }
}
