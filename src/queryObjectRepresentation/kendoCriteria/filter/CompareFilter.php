<?php

namespace dicom\kendoUiQueryBuilder\queryObjectRepresentation\kendoCriteria\filter;

use dicom\kendoUiQueryBuilder\queryObjectRepresentation\field\Field;

/**
 * Class Filter
 *
 * Представляет условие фильтрации:
 * поле, оператор, значение
 *
 * patientId > 35
 * patient.birthDate eq 2011-02-28
 *
 * @package kendoCriteria\filter
 */
class CompareFilter implements ParameterizedFilterInterface
{
    /**
     * "or", "contains", "etc", ...
     * @var string
     */
    protected $operator;

    /**
     * @var mixed
     */
    protected $value;

    /**
     * @var Field
     */
    protected $field;

    /**
     * @var string
     */
    protected $paramName;

    public function __construct(Field $field, $operator, $value)
    {
        $this->setField($field);
        $this->setOperator($operator);
        $this->setValue($value);
    }

    /**
     * @return string
     */
    public function getOperator()
    {
        return $this->operator;
    }

    /**
     * @param string $operator
     */
    public function setOperator($operator)
    {
        $this->operator = $operator;
    }

    /**
     * {@inheritdoc}
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @param mixed $value
     */
    public function setValue($value)
    {
        $this->value = $value;
    }

    /**
     * @return Field
     */
    public function getField()
    {
        return $this->field;
    }

    /**
     * @param Field $field
     */
    public function setField($field)
    {
        $this->field = $field;
    }

    /**
     * {@inheritdoc}
     */
    public function getParam()
    {
        return $this->paramName;
    }

    /**
     * {@inheritdoc}
     */
    public function setParam($paramName)
    {
        $this->paramName = $paramName;
    }
}
