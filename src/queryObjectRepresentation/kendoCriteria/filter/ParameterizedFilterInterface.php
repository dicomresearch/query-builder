<?php

namespace dicom\kendoUiQueryBuilder\queryObjectRepresentation\kendoCriteria\filter;

/**
 * Интерфейс описывает фильтр, условие которого задаётся через параметры
 *
 * @package dicom\kendoUiQueryBuilder\queryObjectRepresentation\kendoCriteria\filter
 */
interface ParameterizedFilterInterface
{
    /**
     * @return mixed Имя параметра
     */
    public function getParam();

    /**
     * @param string Имя параметра
     */
    public function setParam($paramName);
}
