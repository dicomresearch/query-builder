<?php

namespace dicom\kendoUiQueryBuilder\queryObjectRepresentation\kendoCriteria;

/**
 * Генератор имён параметров
 *
 * @package dicom\kendoUiQueryBuilder\queryObjectRepresentation\kendoCriteria
 * @author Ruslan <rsharafutdinov@dicomresearch.com>
 */
class ParameterNameGenerator
{
    /**
     * @var int
     */
    private $counter;

    /**
     * @var string
     */
    private $prefix = 'param';

    /**
     * @param string|null $prefix
     */
    public function __construct($prefix = null)
    {
        $this->counter = 1;

        if ($prefix !== null) {
            $this->prefix = $prefix;
        }
    }

    /**
     * @return string
     */
    public function getParamName()
    {
        return $this->prefix . $this->counter++;
    }
}
