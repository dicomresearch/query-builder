<?php


namespace dicom\kendoUiQueryBuilder\queryObjectRepresentation\exceptions;


use dicom\kendoUiQueryBuilder\exceptions\KendoUiException;

abstract class QueryObjectRepresentationException extends KendoUiException
{

}