<?php

namespace dicom\kendoUiQueryBuilder;

use dicom\kendoUiQueryBuilder\queryObjectRepresentation\field\LinkToRelatedEntity;
use dicom\kendoUiQueryBuilder\queryObjectRepresentation\field\Field;
use dicom\kendoUiQueryBuilder\queryObjectRepresentation\kendoCriteria\filter\CompareFilter;
use dicom\kendoUiQueryBuilder\queryObjectRepresentation\kendoCriteria\filter\ParameterizedFilterInterface;
use dicom\kendoUiQueryBuilder\queryObjectRepresentation\kendoCriteria\KendoCriteriaBuilder;
use dicom\kendoUiQueryBuilder\queryObjectRepresentation\kendoCriteria\logic\LogicalExpression;
use dicom\kendoUiQueryBuilder\queryObjectRepresentation\kendoCriteria\ParameterNameGenerator;
use dicom\kendoUiQueryBuilder\queryObjectRepresentation\KendoOrder\OrderExpressionBuilder;
use dicom\kendoUiQueryBuilder\transformation\DoctrineExpressionFactory;
use dicom\kendoUiQueryBuilder\transformation\operators\logic\LogicExpressionFactory;
use Doctrine\Common\Persistence\Mapping\ClassMetadata;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\QueryBuilder;

/**
 * Class KendoUIQueryBuilder
 *
 * Фасад для конфигурирования QueryBuilder'а значиями сортировки и фильтрации, которые пришли из грида KendoUI
 *
 * @package dicom\kendoUiQueryBuilder
 */
class KendoUIQueryBuilder
{

    /**
     * @var \Doctrine\ORM\Mapping\ClassMetadata
     */
    private $classMetadata;

    /**
     * @var string
     */
    private $entityName;

    /**
     * @var ParameterNameGenerator
     */
    private $paramNameGenerator;

    public function __construct($entityName, ClassMetadata $classMetadata)
    {
        $this->entityName = $entityName;
        $this->classMetadata = $classMetadata;
        $this->paramNameGenerator = new ParameterNameGenerator();
    }

    public function getTableName()
    {
        return $this->classMetadata->getTableName();
    }


    /**
     * Добавить к QueryBuilder'у условия отбора и сортировки из грида KendoUI
     *
     * @param QueryBuilder $queryBuilder
     * @param array $criteria the criteria
     * @param array $orderBy
     * @return QueryBuilder
     */
    public function createQueryBuilder(QueryBuilder $queryBuilder, array $criteria, array $orderBy = [])
    {
        if (count($criteria) > 0) {
            $kendoCriteriaBuilder = new KendoCriteriaBuilder($this->entityName);
            $kendoCriteria = $kendoCriteriaBuilder->buildKendoCriteria($criteria);

            $query = $this->addQueryCriteria($queryBuilder, $kendoCriteria);
            if (null !== $query) {
                $queryBuilder->andWhere($query);
            }
        }

        if (count($orderBy) > 0) {
            $this->addOrder($queryBuilder, $orderBy);
        }

        return $queryBuilder;

    }


    /**
     * Добавляет к QueryBuilder'у условия в прописаные в фильтре filter
     *
     * @param \Doctrine\ORM\QueryBuilder $queryBuilder
     * @param CompareFilter|LogicalExpression $filter
     *
     * @return \Doctrine\ORM\Query\Expr
     */
    protected function addQueryCriteria(QueryBuilder $queryBuilder, $filter)
    {
        $whereQuery = null;
        // пришел ли сложный фильтр
        if ($filter instanceof LogicalExpression) {

            $whereQueries = [];
            foreach ($filter->getFilters() as $subfilter) {
                $criteria =  $this->addQueryCriteria($queryBuilder, $subfilter);
                if (!empty($criteria)) {
                    $whereQueries[] = $criteria;
                }
            }


            if (!empty($whereQueries)) {
                $logicalFactory = new LogicExpressionFactory();
                $whereQuery = $logicalFactory->createDoctrineLogicalExpression($whereQueries, $filter->getLogicalOperator());
            }

        } else {
            if ($filter->getField() instanceof LinkToRelatedEntity) {
                $this->joinTable($queryBuilder, $filter->getField());
            }

            if ($this->isParameterizableFilter($filter)) {
                $filter = $this->parameterizeFilter($filter, $queryBuilder);
            }

            $exprFactory = new DoctrineExpressionFactory();
            $whereQuery = $exprFactory->createExpr($filter);
        }

        return $whereQuery;
    }

    /**
     * Добавляет к QueryBuilder'у условия сортировки
     *
     * @param QueryBuilder $queryBuilder
     * @param $orderBy
     * @return $this
     */
    public function addOrder(QueryBuilder $queryBuilder, $orderBy)
    {
        $orderExpressionBuilder = new OrderExpressionBuilder($this->entityName);
        $orderByAsArray = $orderExpressionBuilder->createManyOrderBy($orderBy);

        foreach ($orderByAsArray as $orderBy) {
            if ($orderBy->getField() instanceof LinkToRelatedEntity) {
                $this->joinTable($queryBuilder, $orderBy->getField());
            }

            $queryBuilder->addOrderBy($orderBy->getField()->getAlias(), $orderBy->getDirection());
        }

        return $this;
    }

    /**
     * Присоединить таблицу
     *
     * @param QueryBuilder $queryBuilder
     * @param Field $field
     * @internal param $filter
     */
    protected function joinTable(QueryBuilder $queryBuilder, Field $field)
    {
        if (!in_array($field->getTableName(), $queryBuilder->getAllAliases())) {
            $queryBuilder->leftJoin(
                $this->getTableName() . '.' . $field->getTableName(),
                $field->getTableName()
            );
        }
    }

    /**
     * @param $filter
     * @return bool
     */
    protected function isParameterizableFilter($filter)
    {
        return $filter instanceof ParameterizedFilterInterface
            && $filter->getOperator() == 'contains';
    }

    /**
     * @param $filter
     * @param QueryBuilder $queryBuilder
     * @return mixed filter
     */
    protected function parameterizeFilter($filter, $queryBuilder)
    {
        $paramName = $this->paramNameGenerator->getParamName();

        $queryBuilder->setParameter($paramName, $filter->getValue());

        $filter->setParam($paramName);
        $filter->setValue(':' . $paramName);

        return $filter;
    }
}